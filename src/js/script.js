$(document).ready(function(){
  // svg4everybody();

  var promoSwiper = new Swiper('.s-promo__slider', {
    speed: 1000,
    slidesPerView: 1,
    loop: true,
    // autoplay: {
    //   delay: 5000,
    // },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    },
  });

  var partnersSwiper = new Swiper('.s-partners__slider', {
    speed: 1000,
    slidesPerView: 5,
    loop: true,
    spaceBetween: 30,
    autoplay: {
      delay: 5000,
    },
    breakpoints: {
      413: {
        slidesPerView: 2,
        spaceBetween: 15
      },
      767: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      1199: {
        slidesPerView: 4,
        spaceBetween: 30
      }
    }
  });

  $('.s-calculate-form__parameters-header-burger').on('click', function() {
    $('.s-calculate-form__parameters-data').toggleClass('s-calculate-form__parameters-data--active');
  });

  $('.s-mobile-landing-header__burger').on('click', function() {
    $('.s-mobile-landing-nav').toggleClass('s-mobile-landing-nav--active');
    $('.s-mobile-landing-header__burger').toggleClass('s-mobile-landing-header__burger--close');
  });

  var gallerySwiper = new Swiper('.s-projects-gallery__slider', {
    effect: 'fade',
    speed: 1000,
    slidesPerView: 1,
    loop: true,
    autoplay: {
      delay: 5000,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    }
  });
});